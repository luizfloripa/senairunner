<?php
/**
 * Created by PhpStorm.
 * User: luiz
 * Date: 20/06/15
 * Time: 13:48
 */

function getDB()
{
    $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "fecam";
    $dbname = "wss";

    $mysql_conn_string = "mysql:host=$dbhost;dbname=$dbname";
    $dbConnection = new PDO($mysql_conn_string, $dbuser, $dbpass);
    $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $dbConnection;
}


require 'vendor/autoload.php';
require 'Slagger/Slagger.php';

$app = new \Slim\Slim();
$app->get('/', function() use($app) {
    $app->response->setStatus(200);
    echo "Welcome to SenaiRunner API";
});


$app->get('/runners', 'getRunners');
$app->get('/runners/:id',  'getRunner');
//$app->get('/runners/search/:query', 'findRunnerByName');
$app->post('/runners', 'addRunner');
$app->put('/runners/:id', 'updateRunner');
$app->delete('/runners/:id',   'deleteRunner');

$app->get('/runs', 'getRuns');
$app->get('/runs/:id',  'getRun');
$app->get('/runs/search/:query', 'findRunByName');
$app->post('/runs', 'addRun');
$app->put('/runs/:id', 'updateRun');
$app->delete('/runs/:id',   'deleteRun');

//corredores inscritos
$app->get('/runs/:id/runners',  'getRunEntries');
$app->get('/runs/:id/runner/:idRunner',  'getRunEntry');
$app->post('/runs/:id/runner/:idRunner',  'addRunEntry');
$app->put('/runs/:id/runner/:idRunner',  'updateRunEntry');
$app->delete('/runs/:id/runner/:idRunner',  'deleteRunEntry');

function getRunners() {
    $sql = "select * FROM corredor ORDER BY nome";
    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        $wines = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo '{"reply": ' . json_encode($wines) . '}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function getRunner($id) {
    $app = \Slim\Slim::getInstance();
    $sql = "SELECT * FROM corredor WHERE id=:id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $id);
        $stmt->execute();
//        $rows = $stmt->fetch(PDO::FETCH_NUM);
        $wine = $stmt->fetchObject();
//        $wine = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;

        if(is_object($wine)){
            echo json_encode($wine);
        }
        else {
            $app->response()->status(404);
            echo '{"error":{"text":Not Found}}';
        }

    } catch(PDOException $e) {
        $app->response()->status(404);
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function addRunner() {
    $request = Slim::getInstance()->request();
    $wine = json_decode($request->getBody());
    $sql = "INSERT INTO corredor (nome, dataNasc, cidade, estado, status) VALUES (:nome, :dataNasc, :cidade, :estado, :status)";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("nome", $wine->nome);
        $stmt->bindParam("dataNasc", $wine->dataNasc);
        $stmt->bindParam("cidade", $wine->cidade);
        $stmt->bindParam("estado", $wine->estado);
        $stmt->bindParam("status", $wine->status);
        $stmt->execute();
        $wine->id = $db->lastInsertId();
        $db = null;
        echo json_encode($wine);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function updateRunner($id) {
    $request = Slim::getInstance()->request();
    $body = $request->getBody();
    $wine = json_decode($body);
    $sql = "UPDATE corredor SET nome=:nome, dataNasc=:dataNasc, cidade=:cidade, estado=:estado, status=:status WHERE id=:id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("nome", $wine->nome);
        $stmt->bindParam("dataNasc", $wine->dataNasc);
        $stmt->bindParam("cidade", $wine->cidade);
        $stmt->bindParam("estado", $wine->estado);
        $stmt->bindParam("status", $wine->status);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $db = null;
        echo json_encode($wine);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function deleteRunner($id) {
    $sql = "DELETE FROM corredor WHERE id=:id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $db = null;
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function findRunnerByName($query) {
    $sql = "SELECT * FROM corredor WHERE UPPER(nome) LIKE :query ORDER BY nome";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $query = "%".$query."%";
        $stmt->bindParam("query", $query);
        $stmt->execute();
        $wines = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo '{"reply": ' . json_encode($wines) . '}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function updateRun($id) {
    $request = Slim::getInstance()->request();
    $body = $request->getBody();
    $wine = json_decode($body);
    $sql = "UPDATE corrida SET nome=:nome, dataNasc=:dataNasc, cidade=:cidade, estado=:estado, status=:status WHERE id=:id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("nome", $wine->nome);
        $stmt->bindParam("dataNasc", $wine->dataNasc);
        $stmt->bindParam("cidade", $wine->cidade);
        $stmt->bindParam("estado", $wine->estado);
        $stmt->bindParam("status", $wine->status);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $db = null;
        echo json_encode($wine);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function deleteRun($id) {
    $sql = "DELETE FROM corrida WHERE id=:id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $db = null;
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function findRunByName($query) {
    $sql = "SELECT * FROM corrida WHERE UPPER(nome) LIKE :query ORDER BY nome";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $query = "%".$query."%";
        $stmt->bindParam("query", $query);
        $stmt->execute();
        $wines = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo '{"reply": ' . json_encode($wines) . '}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function getConnection() {
    $dbhost="127.0.0.1";
    $dbuser="root";
    $dbpass="fecam";
    $dbname="wss";
    $dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $dbh;
}

function getCorredores()
{
//    $stmt = getConn()->query("SELECT * FROM corredor");
    $db = getDB();
    $sth = $db->query("SELECT * FROM corredor");
    $categorias = $sth->fetchAll(PDO::FETCH_OBJ);
    echo "{corredores:".json_encode($categorias)."}";
}


