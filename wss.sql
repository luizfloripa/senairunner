-- phpMyAdmin SQL Dump
-- version 4.2.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 21, 2015 at 10:23 PM
-- Server version: 5.5.43-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `wss`
--

-- --------------------------------------------------------

--
-- Table structure for table `corredor`
--

CREATE TABLE IF NOT EXISTS `corredor` (
`id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `dataNasc` date NOT NULL,
  `cidade` varchar(255) NOT NULL,
  `estado` varchar(2) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `corredor`
--

INSERT INTO `corredor` (`id`, `nome`, `dataNasc`, `cidade`, `estado`, `status`) VALUES
(1, '111', '0000-00-00', 'dfdsfsd', 'sc', 1);

-- --------------------------------------------------------

--
-- Table structure for table `corrida`
--

CREATE TABLE IF NOT EXISTS `corrida` (
`id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `data` date NOT NULL,
  `descricao` longtext NOT NULL,
  `estado` varchar(2) NOT NULL,
  `vlrInscricao` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `incricoes`
--

CREATE TABLE IF NOT EXISTS `incricoes` (
  `corredor` int(11) NOT NULL,
  `corrida` int(11) NOT NULL,
  `statusPag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `corredor`
--
ALTER TABLE `corredor`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `corrida`
--
ALTER TABLE `corrida`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `incricoes`
--
ALTER TABLE `incricoes`
 ADD PRIMARY KEY (`corredor`,`corrida`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `corredor`
--
ALTER TABLE `corredor`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `corrida`
--
ALTER TABLE `corrida`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;